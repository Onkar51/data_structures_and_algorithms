#include <stdio.h>
#include <time.h>

void swap(int *a, int *b) {
  int t = *a;
  *a = *b;
  *b = t;
}

void heapify(int arr[], int n, int i) {
	int max = i;
	int leftChild = 2 * i + 1;
	int rightChild = 2 * i + 2;
	if (leftChild < n && arr[leftChild] > arr[max])
		max = leftChild;
	if (rightChild < n && arr[rightChild] > arr[max])
		max = rightChild;
	if (max != i) {
		swap(&arr[i], &arr[max]);
		heapify(arr, n, max);
	}
}


void heapSort(int arr[], int n) {
	for (int i = n / 2 - 1; i >= 0; i--)
		heapify(arr, n, i);
	for (int i = n - 1; i >= 0; i--) {
		swap(&arr[0], &arr[i]);
		heapify(arr, i, 0);
	}
}


void printArray(int array[], int size) {
  for (int i = 0; i < size; ++i) {
    printf("%d  ", array[i]);
  }
  printf("\n");
}

int main() {
	int input_size;
	printf("\nEnter Input Size: ");
	scanf("%d",&input_size);
	int data[input_size];
	int i=0;
	FILE *ptr;
	ptr = fopen("output.txt","r");
	if (ptr==NULL)
		printf("\nFailed to open");
	while((fscanf(ptr,"%d",&data[i]))!=EOF && i<input_size){
		i++;
	}
	
	int n = sizeof(data) / sizeof(data[0]);
	clock_t t;
	t = clock();
	heapSort(data, n);
	t = clock() - t;
	double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
	printf("heapSort() took %f seconds to execute \n", time_taken);
  
  	printf("\nSorted array in ascending order: \n");
  	printArray(data, n);
}
