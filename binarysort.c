#include <stdio.h>
#include <time.h>
#include<stdlib.h>

struct treenode
{
	struct treenode *leftchild;
	int data ;
	struct treenode *rightchild;
};

void insert ( struct treenode **sr, int num )
{
if ( *sr == NULL )
{
*sr = malloc ( sizeof ( struct treenode ) ) ;
 ( *sr ) -> leftchild = NULL ;
 ( *sr ) -> data = num ;
 ( *sr ) -> rightchild = NULL ;
    }
    else
    {
 if ( num < ( *sr ) -> data )
 insert ( &( ( *sr ) -> leftchild ), num ) ;
 else
 insert ( &( ( *sr ) -> rightchild ), num ) ;
    }
}
void inorder ( struct treenode *sr )
{
if ( sr != NULL )
    {
        inorder ( sr -> leftchild ) ;
        printf ( "%d\t", sr -> data ) ;
        inorder ( sr -> rightchild ) ;
    }
}

void printArray(int array[], int size) {
  for (int i = 0; i < size; ++i) {
    printf("%d  ", array[i]);
  }
  printf("\n");
}

int main() {
	struct treenode *bt=NULL;
	int input_size;
	printf("\nEnter Input Size: ");
	scanf("%d",&input_size);
	int data[input_size];
	int i=0;
	FILE *ptr;
	ptr = fopen("output.txt","r");
	if (ptr==NULL)
		printf("\nFailed to open");
	while((fscanf(ptr,"%d",&data[i]))!=EOF && i<input_size){
		insert(&bt,data[i]);
		i++;
	}
	
	int n = sizeof(data) / sizeof(data[0]);
	clock_t t;
	t = clock();
	inorder(bt);
	t = clock() - t;
	double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds
	printf("binarySort() took %f seconds to execute \n", time_taken);
  
}
