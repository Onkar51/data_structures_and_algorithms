#include <stdio.h>
#include <stdlib.h>

int front=-1, rear=-1;

void add(int item, int V, int q[V]) {
  if (rear == (V-1))
    printf("QUEUE FULL");
  else {
    if (rear == -1) {
      q[++rear] = item;
      front++;
    } else
      q[++rear] = item;
  }
}

int delete(int V,int q[V]) {
  int k;
  if ((front > rear) || (front == -1))
    return (0);
  else {
    k = q[front++];
    return (k);
  }
}

void BFS(int s, int V, int visited[V],int a[V][V], int q[V]) 
{	
	int p, i;
	add(s, V, q);
	visited[s] = 1;
	p = delete(V, q);
	if (p != 0)
	    printf(" %d", p);
	while (p != 0) {
		p = delete(V, q);
	    for (i = 0; i < V; i++)
	    	if ((a[p][i] != 0) && (visited[i] == 0)) 
	    	{
	 		add(i, V, q);
			visited[i] = 1;
	        }
	    if (p != 0)
	      printf(" %d ", p);
	}
	for (i = 0; i < V; i++)
	    if (visited[i] == 0)
	    	BFS(i, V, visited, a, q);
}

void DFS(int i,int V, int visited[], int a[V][V])
{
	int j;
	printf("%d ",i);
	visited[i]=1;
	for(j=0;j<V;j++)
	{
		if(!visited[j]&&a[i][j]==1)
			DFS(j, V, visited, a);
	}
}

int main()
{
	int V, E;
	printf("\nEnter no of vertices: ");
	scanf("%d",&V);
	int a[V][V];
	for(int i=0;i<V;i++)
	{
		for(int j=0;j<V;j++)
		{
			a[i][j]=0;
		}
	}
	printf("\nEnter no of edges: ");
	scanf("%d",&E);
	int v1,v2;
	for(int i=0;i<E;i++)
	{
		printf("\nEnter edge: ");
		scanf("%d %d",&v1,&v2);
		if(v1>V || v2>V)
		{
			printf("Incorrect input");
			exit(0);
		}
		else if(a[v1][v2]==0 || a[v2][v1]==0)
		{
			a[v1][v2]=a[v2][v1]=1;
		}
	}
	
	//DFS traversal code
	int visited[V];
	for(int i=0;i<V;i++)
	{
		visited[i]=0;
	}
	printf("\nDFS Traversal: ");
	DFS(0, V, visited, a);
	
	//BF traversal code
	int q[V];
	for(int i=0;i<V;i++)
	{
		visited[i]=0;
	}
	printf("\nBFS Traversal: 0");
	BFS(0, V, visited, a, q);
	return 0;
}
